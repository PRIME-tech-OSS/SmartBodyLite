# Maintained by [InnerPiece](https://innerpiece.io)
Goals:
- Standard formats support: .bvh skeletons and motion files
- Focus on core features: generate animations for characters
- Make it smaller, faster and easy to use
- Remove unnecessary parts

# Building
(tested on Ubuntu 18.04 only)
- install these packages: <br />
sudo apt-get install libxerces-c3-dev libgl1-mesa-dev libglu1-mesa-dev xutils-dev libxi-dev freeglut3-dev libglew-dev libxft-dev libapr1-dev libaprutil1-dev libcppunit-dev liblapack-dev libblas-dev libf2c2-dev build-essential mono-devel mono-xbuild python-dev libopenal-dev libsndfile-dev libalut-dev libfltk1.3-dev libboost-all-dev libode-dev libprotobuf-dev
- run `linuxlibsbuild.sh`
- mkdir build
- cd build
- cmake .. -DCMAKE_BUILD_TYPE=Release
- make -j4 install
- cd ..
- cd bin
- ./irrlichtsmartbody <br />
Wait for a minute since the Python API is slow <br />
The application will set up some monsters and make them walk/run around <br />
Notice that characters won't bump into each other
- Click to this image to see demo video <br />
[![](https://i.imgur.com/Xcawhbt.jpg)](https://www.youtube.com/watch?v=TEL3zloebjo)

# Todo
- Remove more dependencies

# Original Readme.md below

# Author

11/23/15

Ari Shapiro, Ph.D.

Download
----------
The linux SDK is available for download from the SmartBody site:

http://smartbody.ict.usc.edu/download

Build instructions for SmartBody on linux:
-------------------------------------------
The SmartBody SDK for linux is in source form and requires a number of supporting libraries to build
properly. A full description is located in the SmartBody Manual (SmartBodyManual.pdf). Here's a summary when using Ubuntu:

Preparation
-----------------

# packages needed for basic build
sudo apt-get install cmake g++ libxerces-c3-dev libgl1-mesa-dev libglu1-mesa-dev xutils-dev libxi-dev freeglut3-dev libglew-dev libxft-dev libapr1-dev libaprutil1-dev libcppunit-dev liblapack-dev libblas-dev libf2c2-dev build-essential mono-devel mono-xbuild python-dev libopenal-dev libsndfile-dev libalut-dev ncurses-dev fltk1.3-dev

#packages needed for Ogre rendering
sudo apt-get install libzzip-dev libxaw7-dev libxxf86vm-dev libxrandr-dev libfreeimage-dev nvidia-cg-toolkit libois-dev libogre-1.9-dev ogre-1.9-samples-data

# irrlicht code is included in the sdk

In addition, the activemq-cpp libraries will need to be installed.
The source code is included in this distribution. Other versions can be found here:
http://activemq.apache.org/cms/


Build
-------------------
First, build the dependencies by running:

./linuxbuildlibs.sh

This will install the needed headers into the ./include folder, and the libraries into the ./lib folder.

Next, build SmartBody.
The build uses the cmake system as follows:

# make a directory called 'mybuild' and generate the makefiles for SmartBody there
mkdir mybuild
cd mybuild
cmake ..

# do the build
make install
# alternatively, you can build using multiple threads like:
# make -j8 install

If you want to build the Ogre-SmartBody or the Irrlicht-SmartBody example code, then uncomment those lines at the bottom of the file src/CMakeLists.txt then rebuild


Running SmartBody
--------------------
SmartBody can be used as a library, or run directory from the front-end application called sbgui located in the bin/ directory:

# running sbgui
cd bin
./sbgui

There is an example of using the Ogre renderer with SmartBody called ogresmartbody which can be run:
cd bin
./ogresmartbody

There is an example of using the Irrlicht game engine with SmartBody called irrlichtsmartbody which can be run:
cd bin
./irrlichtsmartbody

Please feel free to post comments and questions on the SmartBody forum:

http://smartbody.ict.usc.edu/forum



3rd party licenses
-------------------
A list of 3rd party licenses is located in the file:
3rd party licenses.txt
The entire SmartBody distribution is available for download from SourceForge:
svn checkout svn://svn.code.sf.net/p/smartbody/code/sdk smartbodysdk

