#include "vhcl.h"
#define SB_NO_PYTHON
#include <sb/SBScene.h>
#include <sb/SBCharacter.h>
#include <sb/SBSkeleton.h>
#include <sb/SBSimulationManager.h>
#include <sb/SBBmlProcessor.h>
#include <sb/SBSceneListener.h>
#include <sb/SBAssetManager.h>
#include <sb/SBJointMapManager.h>
#include <sb/SBJointMap.h>

class SimpleListener : public SmartBody::SBSceneListener
{
   public:
	   SimpleListener() {}
	   ~SimpleListener() {}
     
	  virtual void OnLogMessage( const std::string & message )
	  {
#ifdef WIN32
		LOG("%s", message.c_str());
#else
		std::cout << message << std::endl;
#endif
	  }
};

void createJointMap(SmartBody::SBScene* scene) {
	SmartBody::SBJointMap* map = scene->getJointMapManager()->createJointMap("bvh_map");
	//  map->setMapping("spine", "base");
	map->setMapping("LowerBack", "spine1");
  map->setMapping("Spine", "spine2");
  map->setMapping("Spine1", "spine3");
  map->setMapping("Neck", "spine4");
  map->setMapping("Neck1", "spine5");
  map->setMapping("Head", "skullbase");

  // Arm, left
  map->setMapping("LeftShoulder", "l_sternoclavicular");
  map->setMapping("LeftArm", "l_shoulder");
//  map->setMapping("JtUpperArmTwistALf", "l_upperarm1");
//  map->setMapping("JtUpperArmTwistBLf", "l_upperarm2");
  map->setMapping("LeftForeArm", "l_elbow");
//  map->setMapping("JtForearmTwistALf", "l_forearm1");
//  map->setMapping("JtForearmTwistBLf", "l_forearm2");
  map->setMapping("LeftHand", "l_wrist");
  map->setMapping("LThumb", "l_thumb1");
  // map->setMapping("thumb.02.L", "l_thumb2");
  // map->setMapping("thumb.03.L", "l_thumb3");
//  map->setMapping("JtThumbDLf", "l_thumb4");
  // map->setMapping("palm.01.L", "l_index1");
  // map->setMapping("f_index.01.L", "l_index2");
  // map->setMapping("f_index.02.L", "l_index3");
  // map->setMapping("f_index.03.L", "l_index4");
  // map->setMapping("palm.02.L", "l_middle1");
  // map->setMapping("f_middle.01.L", "l_middle2");
  // map->setMapping("f_middle.02.L", "l_middle3");
  // map->setMapping("f_middle.03.L", "l_middle4");
  // map->setMapping("palm.03.L", "l_ring1");
  // map->setMapping("f_ring.01.L", "l_ring2");
  // map->setMapping("f_ring.02.L", "l_ring3");
  // map->setMapping("f_ring.03.L", "l_ring4");
  // map->setMapping("palm.04.L", "l_pinky1");
  // map->setMapping("f_pinky.01.L", "l_pinky2");
  // map->setMapping("f_pinky.02.L", "l_pinky3");
  // map->setMapping("f_pinky.03.L", "l_pinky4");

  // Arm, right
	map->setMapping("RightShoulder", "r_sternoclavicular");
  map->setMapping("RightArm", "r_shoulder");
//  map->setMapping("JtUpperArmTwistALf", "l_upperarm1");
//  map->setMapping("JtUpperArmTwistBLf", "l_upperarm2");
  map->setMapping("RightForeArm", "r_elbow");
//  map->setMapping("JtForearmTwistALf", "l_forearm1");
//  map->setMapping("JtForearmTwistBLf", "l_forearm2");
  map->setMapping("RightHand", "r_wrist");
  map->setMapping("RThumb", "r_thumb1");
  // map->setMapping("thumb.02.L", "l_thumb2");
  // map->setMapping("thumb.03.L", "l_thumb3");
//  map->setMapping("JtThumbDLf", "l_thumb4");
  // map->setMapping("palm.01.L", "l_index1");
  // map->setMapping("f_index.01.L", "l_index2");
  // map->setMapping("f_index.02.L", "l_index3");
  // map->setMapping("f_index.03.L", "l_index4");
  // map->setMapping("palm.02.L", "l_middle1");
  // map->setMapping("f_middle.01.L", "l_middle2");
  // map->setMapping("f_middle.02.L", "l_middle3");
  // map->setMapping("f_middle.03.L", "l_middle4");
  // map->setMapping("palm.03.L", "l_ring1");
  // map->setMapping("f_ring.01.L", "l_ring2");
  // map->setMapping("f_ring.02.L", "l_ring3");
  // map->setMapping("f_ring.03.L", "l_ring4");
  // map->setMapping("palm.04.L", "l_pinky1");
  // map->setMapping("f_pinky.01.L", "l_pinky2");
  // map->setMapping("f_pinky.02.L", "l_pinky3");
  // map->setMapping("f_pinky.03.L", "l_pinky4");


  // Leg, left
  map->setMapping("LeftUpLeg", "l_hip");
  map->setMapping("LeftLeg", "l_knee");
  map->setMapping("LeftFoot", "l_ankle");
  map->setMapping("LeftToeBase", "l_forefoot");
//  map->setMapping("toe.L", "l_toe");

  // Leg, right
  map->setMapping("RightUpLeg", "r_hip");
  map->setMapping("RightLeg", "r_knee");
  map->setMapping("RightFoot", "r_ankle");
  map->setMapping("RightToeBase", "r_forefoot");
//  map->setMapping("toe.R", "r_toe");

  // Head, left
//  map->setMapping("JtEyeLf", "eyeball_left");
//  map->setMapping("JtEyelidUpperLf", "upper_eyelid_left");
//  map->setMapping("JtEyelidLowerLf", "lower_eyelid_left");

  // Head, right
//  map->setMapping("JtEyeRt", "eyeball_right");
//  map->setMapping("JtEyelidUpperRt", "upper_eyelid_right");
//  map->setMapping("JtEyelidLowerRt", "lower_eyelid_right");

//  map->setMapping("eyeJoint_R", "eyeball_right");
//  map->setMapping("eyeJoint_L", "eyeball_left");
}

int main( int argc, char ** argv )
{
	// set the relative path from the location of the simplesmartbody binary to the data directory
	// if you are downloading the source code from SVN, it will be ../../../../data
	//std::string mediaPath = "../../../../data";
	// if you're using the SDK, this path will be ../data
	std::string mediaPath = "../data";

	// add a message logger to stdout
	vhcl::Log::StdoutListener* stdoutLog = new vhcl::Log::StdoutListener();
	vhcl::Log::g_log.AddListener(stdoutLog);

	// get the scene object
	SmartBody::SBScene* scene = SmartBody::SBScene::getScene();
	SimpleListener listener;
	scene->addSceneListener(&listener);

	// set the mediapath which dictates the top-level asset directory
	scene->setMediaPath(mediaPath);

	// indicate where different assets will be located
	// "motion" = animations and skeletons
	// "script" = Python scripts to be executed
	// "mesh" = models and textures
	scene->addAssetPath("motion", "behaviorsets/CMUMocap/skeletons");
	scene->addAssetPath("motion", "behaviorsets/CMUMocap/motions");

	// load the assets from the indicated locations
	LOG("Loading Assets...");
	scene->loadAssets();
	int numMotions = scene->getNumMotions();
	LOG("Loaded %d motions...", numMotions);

	// create a character
	LOG("Creating the character...");
	SmartBody::SBCharacter* character = scene->createCharacter("mycharacter", "");

	// load the skeleton from one of the available skeleton types
	SmartBody::SBSkeleton* skeleton = scene->createSkeleton("cmu.bvh");
	createJointMap(scene);
	SmartBody::SBJointMap* jointMap = scene->getJointMapManager()->getJointMap("bvh_map");
	jointMap->applySkeleton(skeleton);

	SmartBody::SBAssetManager* am = scene->getAssetManager();
	std::vector<std::string> names = am->getMotionNames();
	std::vector<std::string> skels = am->getSkeletonNames();

	// attach the skeleton to the character
	character->setSkeleton(skeleton);

	// create the standard set of controllers (idling, gesture, nodding, etc.)
	character->createStandardControllers();

	// get the simulation object 
	SmartBody::SBSimulationManager* sim = scene->getSimulationManager();

	// if you want to use a real-time clock do the following:
	bool useRealTimeClock = true;
	if (useRealTimeClock)
	{
		sim->setupTimer();
	}
	else
	{
		// otherwise, the time will run according
		sim->setTime(0.0);
	}

	// make the character do something
	scene->getBmlProcessor()->execBML("mycharacter", "<body posture=\"cmu_walk_02_01.bvh\"/>");
	
	LOG("Starting the simulation...");
	double lastPrint = 0;
	sim->start();
	while (sim->getTime() < 100.0) // run for 100 simulation seconds
	{
		scene->update();
		if (!useRealTimeClock)
			sim->setTime(sim->getTime() + 0.16); // update at 1/60 of a second when running in simulated time
		else
			sim->updateTimer();
		

		if (sim->getTime() > lastPrint)
		{
			LOG("Simulation is at time: %5.2f\n", sim->getTime());
			lastPrint = sim->getTime() + 10;
		}

		const std::vector<std::string>& characterNames = scene->getCharacterNames();
		for (size_t c = 0; c < characterNames.size(); c++)
		{
			SmartBody::SBCharacter* character = scene->getCharacter(characterNames[c]);
			std::string jointName = "Hips";
			SmartBody::SBJoint* joint = character->getSkeleton()->getJointByName(jointName);
			if (joint)
			{
				SrVec position = joint->getPosition();
				LOG("Character %s joint %s is at position (%f, %f, %f)", character->getName().c_str(), jointName.c_str(), position.x, position.y, position.z);
			}
		}
	}

	sim->stop();

	
	
	return 0;
}
